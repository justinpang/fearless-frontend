import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conferences: [],
            conference: '',
        };
        this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleChangeConference = this.handleChangeConference.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount(){
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }
    handlePresenterNameChange(event){
        const value = event.target.value;
        this.setState({presenter_name: value})
    }
    handleChangeEmail(event){
        const value = event.target.value;
        this.setState({presenter_email: value})
    }
    handleCompanyNameChange(event){
        const value = event.target.value;
        this.setState({company_name: value})
    }
    handleTitleChange(event){
        const value = event.target.value;
        this.setState({title: value})
    }
    handleSynopsisChange(event){
        const value = event.target.value;
        this.setState({synopsis: value})
    }
    handleChangeConference(event){
        const value = event.target.value;
        this.setState({conference: value})
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        
        const presentationUrl = `http://localhost:8000/api/conferences/${this.state.conference}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            this.setState({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
                conferences: [],
              });
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Presentation</h1>
                    <form onSubmit={this.handleSubmit} id="create-presentation-form">
                    <div className="form-floating mb-3">
                        <input value={this.state.presenter_name} onChange={this.handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                        <label htmlFor="presenter_name">Presenter Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={this.state.presenter_email} onChange={this.handleChangeEmail} required placeholder="Presenter email" type="email" name="presenter_email" id="presenter_email" className="form-control" />
                        <label htmlFor="presenter_email">Presenter Email</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={this.state.company_name} onChange={this.handleCompanyNameChange} placeholder="Company name" required type="text" name="company_name" id="company_name" className="form-control" />
                        <label htmlFor="company_name">Company Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={this.state.title} onChange={this.handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                        <label htmlFor="title">Title</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="synopsis">Synopsis</label>
                        <textarea value={this.state.synopsis} onChange={this.handleSynopsisChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeConference} name="conference" id="conference" className="form-select" required>
                        <option value="">Choose a conference</option>
                        {this.state.conferences.map(conference => {
                            return (
                            <option key={conference.href} value={conference.id}>{conference.name}</option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        );
    }
}

export default PresentationForm;